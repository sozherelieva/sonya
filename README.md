# README #

### Repository Guide ###

* task1 - BigInteger
* task2 - Z, Prefix function, Z <-> Prefix
* task3 - find pattern with gaps in the text (Bor)
* module2task1 - number of different substrings in the text. time O(nlogn), memory O(n)
* module3_task1 - distance between segments
* module3_task2 - R^3 convex hull
* module3_task3 - polygons intersection, R^2 (Minkowski sum)