#include <algorithm>
#include <iostream>

using std::string;
using std::vector;
using std::pair;
using std::make_pair;

// Add symbol $
#define ALPHABETSIZE 27
#define FIRSTSYMBOL 'a'

char terminalSymbol = FIRSTSYMBOL - 1;


class CSuffixArray{
public:
    explicit CSuffixArray(const string& new_string);
    const vector<int>& returnSuffixArray();
private:
    string text;
    const int textSize;
    vector<int> suffixArray;
    vector<int> eqvRelation;

    void firstSymbolsSort();
    void sortPairs(vector<pair<int, pair<int, int>>>& eqvPairs);
    void updateClasses(const vector<pair<int, pair<int, int>>>& eqvPairs);
    void updateSuffixArray(const vector<pair<int, pair<int, int>>>& eqvPairs);
    void buildSuffixArray();
};

CSuffixArray::CSuffixArray(const string& new_string)
        : text(new_string), textSize(static_cast<int>(text.size())) {
    eqvRelation.resize(text.size());
    suffixArray.resize(text.size());
    buildSuffixArray();
}

const vector<int>& CSuffixArray::returnSuffixArray() {
    return suffixArray;
}

// Устойчивая сортировка подсчетом
void CSuffixArray::firstSymbolsSort() {
    vector<int> symbols;
    symbols.resize(ALPHABETSIZE);
    for (char symbol:text) {
        symbols[symbol - terminalSymbol]++;
    }
    for (int i = 1; i < ALPHABETSIZE; i++) {
        symbols[i] += symbols[i - 1];
    }
    for (int i = textSize - 1; i >= 0; i--) {
        suffixArray[--symbols[text[i] - terminalSymbol]] = i;
    }
}

// Устойчивая сортировка подсчетом сначала по вторым элементам, потом по первым
void CSuffixArray::sortPairs(vector<pair<int, pair<int, int>>>& eqvPairs ) {
    vector<pair<int, pair<int, int>>> newEqvPairs;
    newEqvPairs.resize(text.size());
    vector<int> counterFirst;
    vector<int> counterSecond;
    counterFirst.resize(textSize);
    counterSecond.resize(textSize);
    for (pair<int, pair<int, int>> pair:eqvPairs) {
        counterFirst[pair.second.first]++;
        counterSecond[pair.second.second]++;
    }
    for (int i = 1; i < textSize; i++) {
        counterFirst[i] += counterFirst[i - 1];
        counterSecond[i] += counterSecond[i - 1];
    }
    for (int i = textSize - 1; i >= 0; i--) {
        newEqvPairs[--counterSecond[eqvPairs[i].second.second]] = eqvPairs[i];
    }
    for(int i = 0; i < textSize; i++){
        eqvPairs[i] = newEqvPairs[i];
    }
    for (int i = textSize - 1; i >= 0; i--) {
        newEqvPairs[--counterFirst[eqvPairs[i].second.first]] = eqvPairs[i];
    }
    for(int i = 0; i < textSize; i++){
        eqvPairs[i] = newEqvPairs[i];
    }
}

void CSuffixArray::updateClasses(const vector<pair<int, pair<int, int>>>& eqvPairs) {
    int classes = 0;
    for (int i = 1; i < textSize; i++) {
        if (eqvPairs[i].second != eqvPairs[i - 1].second) {
            ++classes;
        }
        eqvRelation[eqvPairs[i].first] = classes;
    }
}

void CSuffixArray::updateSuffixArray(const vector<pair<int, pair<int, int>>>& eqvPairs) {
    for(int i = 0; i < textSize; i++){
        suffixArray[i] = eqvPairs[i].first;
    }
}

void CSuffixArray::buildSuffixArray() {
    firstSymbolsSort();
    eqvRelation[suffixArray[0]] = 0;
    int classes = 0;
    for (int i = 1; i < textSize; i++) {
        if (text[suffixArray[i]] != text[suffixArray[i - 1]]) {
            ++classes;
        }
        eqvRelation[suffixArray[i]] = classes;
    }

    for (int i = 1; i < textSize; i *= 2) {
        int counter = 0;
        vector<pair<int, pair<int, int>>> eqvPairs;
        eqvPairs.resize(text.size());
        for (int suffix: suffixArray ) {
            int step = (suffix + i) % textSize;
            pair<int, int> eqvPair = make_pair(eqvRelation[suffix], eqvRelation[step]);
            eqvPairs[counter] = make_pair(suffix, eqvPair);
            counter++;
        }
        sortPairs(eqvPairs);
        updateSuffixArray(eqvPairs);
        updateClasses(eqvPairs);
    }
}

class CLCP{
public:
    CLCP(const string& newString, vector<int>&& newSuffixArray);
    const vector<long long>& returnLCP();
    const vector<int>& returnInverseSuffixArray();
private:
    const string text;
    int textSize;
    const vector<int> suffixArray;
    vector<long long> lcp;
    vector<int> inverseSuffixArray;

    void buildLCP();
};

CLCP::CLCP(const string& newString, vector<int>&& newSuffixArray)
        :text(newString), suffixArray(newSuffixArray), textSize(static_cast<int>(text.size())) {
    lcp.resize(text.size());
    inverseSuffixArray.resize(text.size());
    for(int i = 0; i < textSize; i++){
        inverseSuffixArray[suffixArray[i]] = i;
    }
    buildLCP();
}

const vector<long long>& CLCP::returnLCP() {
    return lcp;
}

const vector<int>& CLCP::returnInverseSuffixArray() {
    return inverseSuffixArray;
}

void CLCP::buildLCP() {
    long long prefix = 0;
    for (int current = 0; current < textSize; current++) {
        if (prefix > 0) {
            prefix--;
        }
        int currentSufPos = inverseSuffixArray[current];
        if (currentSufPos == textSize - 1) {
            prefix = 0;
        }else{
            int next = suffixArray[currentSufPos + 1];
            while(std::max(current+prefix, next+prefix) < textSize && text[current+prefix] == text[next + prefix]){
                prefix++;
            }
            lcp[currentSufPos] = prefix;
        }
    }
}

long long allSubstringsSearch(vector<long long> lcp, vector<int> suffixArray){
    long long result = 0;
    int textSize = static_cast<int>(lcp.size()) - 1;
    result = result + textSize - suffixArray[0];
    for(int i = 1; i < textSize + 1; i++) {
        result = result + textSize - suffixArray[i] - lcp[i - 1];
    }
    return result;
}

void textPreparation(string& text){
    text += terminalSymbol;
}

int main() {
    string text;
    std::cin >> text;

    textPreparation(text);
    CSuffixArray suffixArrayFind(text);
    vector<int> suffixArray = suffixArrayFind.returnSuffixArray();

    CLCP LCP(text, move(suffixArray));
    vector<long long> lcp = LCP.returnLCP();
    vector<int> inverseSuffixArray = LCP.returnInverseSuffixArray();

    long long result = allSubstringsSearch(lcp, suffixArray);
    std::cout << result << '\n';
    return 0;
}