#include <iostream>
#include <vector>
#include <cmath>

using std::cin;
using std::cout;
using std::vector;
using std::acos;


struct Point {
    explicit Point(double new_x = 0.0, double new_y = 0.0);
    friend Point operator+(const Point& a1, const Point& a2);
    friend Point operator-(const Point& a1, const Point& a2);
    friend Point operator-(const Point& a1);
    friend double vectorProduct(const Point& a1, const Point& a2);
    friend double angle(const Point& a, const Point& b);

    double length();

    double x;
    double y;
};

Point::Point(double new_x, double new_y)
        :x(new_x)
        ,y(new_y)
{}

Point operator+(const Point& a1, const Point& a2) {
    return Point(a1.x + a2.x, a1.y + a2.y);
}

Point operator-(const Point& a1, const Point& a2) {
    return Point(a1.x - a2.x, a1.y - a2.y);
}

Point operator-(const Point& a1) {
    return Point(-a1.x, -a1.y);

}

double vectorProduct(const Point& a1, const Point& a2) {
    return a1.x * a2.y - a2.x * a1.y;
}

double Point::length() {
    return sqrt(x*x + y*y);
}

enum TQuarter {Q_FIRST = 1, Q_SECOND = 2, Q_THIRD = 3, Q_FOURTH = 4};

int findQuarter(double cos, double sin){
    if(cos < 0.0){
        if(sin < 0.0){
            return Q_FIRST;
        }else {
            return Q_FOURTH;
        }
    }
    if(cos > 0.0){
        if(sin > 0.0){
            return Q_THIRD;
        }else {
            if(sin == 0.0){
                return Q_THIRD;
            }
            return Q_SECOND;
        }
    }else {
        if(sin > 0.0){
            return Q_THIRD;
        } else {
            return Q_FIRST;
        }
    }
}

double angle(const Point& a, const Point& b){
    double cos, sin, angle = 0.0;
    Point z = b - a;
    cos = z.x/z.length();
    sin = z.y/z.length();
    int quarter = findQuarter(cos, sin);
    switch (quarter){
        case Q_FIRST:
            angle = acos(-cos);
            break;
        case Q_SECOND:
            angle = acos(-cos);
            break;
        case Q_THIRD:
            angle = asin(sin);
            break;
        case Q_FOURTH:
            angle = acos(cos);
            break;
        default:
            break;
    }
    if(quarter == Q_FIRST || quarter == Q_SECOND){
        angle += M_PI;
    }
    return angle;
}

// Minkowski sum
void sum (vector<Point>& A, vector<Point>& B, vector<Point>& answer) {
    int i = 0, j = 0;
    int i_max = static_cast<int>(A.size());
    int j_max = static_cast<int>(B.size());
    A.push_back(A[0]);
    A.push_back(A[1]);
    B.push_back(B[0]);
    B.push_back(B[1]);
    while (i < i_max || j < j_max) {
        answer.push_back(A[i] + B[j]);
        double angle1 = angle(A[i], A[i+1]);
        double angle2 = angle(B[j], B[j+1]);
        if(angle1 < angle2){
            if(i != i_max){
                i++;
            }else{
                j++;
            }
        }else if(angle1 > angle2){
            if(j != j_max){
                j++;
            }else{
                i++;
            }
        }else{
            i++;
            j++;
        }
    }
    A.pop_back();
    A.pop_back();
    B.pop_back();
    B.pop_back();
}

//set the bottom left point to the beginning, clockwise
void sort(vector<Point>& A){
   int bottom_left_point = 0;
    vector<Point> B;
    B.resize(A.size());
    for(int i = 1; i < static_cast<int>(A.size()); i++){
        if(A[bottom_left_point].y > A[i].y){
            bottom_left_point = i;
        }
        if(A[bottom_left_point].y == A[i].y && A[bottom_left_point].x > A[i].x){
            bottom_left_point = i;
        }
    }
    B[0] = A[bottom_left_point];
    int j = 0;
    for(int i = bottom_left_point; i >= 0; i--){
        B[j] = A[i];
        j++;
    }
    for(int i = static_cast<int>(A.size()) - 1; i > bottom_left_point; i--){
        B[j] = A[i];
        j++;
    }
    A = B;
}

bool intersection(vector<Point>& A, vector<Point>& B) {
    for(int i = 0; i < A.size(); i++){
        A[i] = -A[i];
    }
    sort(A);
    sort(B);
    vector<Point> answer;
    sum(A, B, answer);
    answer.push_back(answer[0]);
    int counter = 0;
    for(int i = static_cast<int>(answer.size()) - 1; i > 0; i--){
        if(vectorProduct(answer[i - 1] - answer[i],-answer[i]) > 0){
            counter++;
        }
    }
    return counter == static_cast<int>(answer.size()) - 1 || counter == 0;
}


int main() {
    unsigned int number_of_dots1, number_of_dots2;

    cin >>  number_of_dots1;
    vector<Point> polygon1;
    polygon1.resize(number_of_dots1);
    for(int i = 0; i < number_of_dots1; i++){
        double x, y;
        cin >> x >> y;
        polygon1[i] = Point(x, y);
    }

    cin >>  number_of_dots2;
    vector<Point> polygon2;
    polygon2.resize(number_of_dots2);
    for(int i = 0; i < number_of_dots2; i++){
        double x, y;
        cin >> x >> y;
        polygon2[i] = Point(x, y);
    }

    if(intersection(polygon1, polygon2)){
        cout << "YES";
    }else{
       cout << "NO";
    }
    return 0;
}
