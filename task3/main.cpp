#include <fstream>
#include <iostream>
#include <memory>
#include <queue>

#define DIVIDER '?'
#define ALPHABET_SIZE 26
#define FIRST_SYMBOL 'a'

using std::vector;
using std::string;
using std::weak_ptr;
using std::shared_ptr;
using std::make_shared;

struct CBorVertex {
    bool is_leaf;
    vector<int> pattern_number;
    weak_ptr<CBorVertex> parent;
    char char_to_parent;
    weak_ptr<CBorVertex> suffix_link;
    weak_ptr<CBorVertex> compressed_link;
    vector<weak_ptr<CBorVertex>> jump;
    //next_vertices[i] - pointer to the vertex in which you can go along the edge 'i'.
    vector<shared_ptr<CBorVertex>> next_vertices;
    CBorVertex();
};

CBorVertex::CBorVertex() {
    is_leaf = false;
    char_to_parent = 0;
    jump.resize(ALPHABET_SIZE);
    next_vertices.resize(ALPHABET_SIZE);
}

class CBorTree {
public:
    explicit CBorTree(const vector<string>& little_patterns, string&& new_text, int question_end);

    vector<int> FindPattern(const vector<int>& start_pat_pos);
private:
    vector<int> pattern_lenght;
    int question_mark_end;

    shared_ptr<CBorVertex> root;
    string text;

    // Add pattern to bor.
    void AddPattern(const string& pattern, int pattern_number);

    weak_ptr<CBorVertex> GetSuffixLink(shared_ptr<CBorVertex> vertex);
    weak_ptr<CBorVertex> GetLink(shared_ptr<CBorVertex> vertex, char character);
    weak_ptr<CBorVertex> GetCompressedLink(shared_ptr<CBorVertex> vertex);
};

CBorTree::CBorTree(const vector<string>& little_patterns, string&& new_text, int question_end) {
    root = make_shared<CBorVertex>();

    question_mark_end = question_end;
    text = std::move(new_text);
    int pattern_number = 0;
    // Add pattern to bor (built bor).
    for (auto& new_pattern: little_patterns) {
        pattern_lenght.push_back(static_cast<int>(new_pattern.size()));
        AddPattern(new_pattern, pattern_number);
        pattern_number++;
    }
}

void CBorTree::AddPattern(const string& pattern, int pattern_number) {
    weak_ptr<CBorVertex> vertex = root;
    auto vertex_shr = vertex.lock();

    for (auto symbol: pattern) {
        int position = symbol - FIRST_SYMBOL;
        // Add next node to bor.
        if (vertex_shr->next_vertices[position].get() == nullptr) {
            auto new_vertex = make_shared<CBorVertex>();

            new_vertex->parent = vertex;
            new_vertex->char_to_parent = symbol;
            vertex_shr->next_vertices[position] = new_vertex;
        }
        vertex = vertex_shr->next_vertices[position];
        vertex_shr = vertex.lock();
    }
    // The end of the pattern.
    vertex_shr = vertex.lock();
    vertex_shr->is_leaf = true;
    vertex_shr->pattern_number.push_back(pattern_number);
}

weak_ptr<CBorVertex> CBorTree::GetSuffixLink(shared_ptr<CBorVertex> vertex) {
    if(vertex->suffix_link.expired()) {
        auto parent = vertex->parent.lock();
        // vertex->parent.expired() it's like vertex == root
        if(vertex->parent.expired() || parent->parent.expired()) {
            vertex->suffix_link = root;
        } else {
            vertex->suffix_link = GetLink(GetSuffixLink(vertex->parent.lock()).lock(), vertex->char_to_parent);
        }
    }
    return vertex->suffix_link;
}

weak_ptr<CBorVertex> CBorTree::GetLink(shared_ptr<CBorVertex> vertex, char character) {
    int position = character - FIRST_SYMBOL;
    if(vertex->jump[position].expired()){
        if(vertex->next_vertices[position] != nullptr){
            vertex->jump[position] = vertex->next_vertices[position];
            // vertex->parent.expired() it's like vertex == root
        } else if(vertex->parent.expired()) {
            vertex->jump[position] = root;
        } else{
            vertex->jump[position] = GetLink(GetSuffixLink(vertex).lock(), character);
        }
    }
    return vertex->jump[position];
}

weak_ptr<CBorVertex> CBorTree::GetCompressedLink(shared_ptr<CBorVertex> vertex) {
    if (vertex->compressed_link.expired()) {
        weak_ptr<CBorVertex> new_vertex = GetSuffixLink(vertex);
        if (new_vertex.lock()->is_leaf) {
            vertex->compressed_link = new_vertex;
        } else if (new_vertex.lock()->parent.expired()) {
            vertex->compressed_link = root;
        } else {
            vertex->compressed_link = GetCompressedLink(new_vertex.lock());
        }

    }
    return vertex->compressed_link;
}

vector<int> CBorTree::FindPattern(const vector<int>& start_pat_pos) {
    vector<int> result;
    vector<int> positions(text.size(), 0);
    weak_ptr<CBorVertex> current;
    current = root;
    for (int i = 0; i < static_cast<int>(text.size()); i++) {
        current = GetLink(current.lock(), text[i]);
        weak_ptr<CBorVertex> temp = current;
        auto temp_shr = temp.lock();
        // like temp_shr != root
        while (!temp_shr->parent.expired()) {
            if (temp_shr->is_leaf) {
                // Add little pattern (without ?) positions in text
                for (auto num: temp_shr->pattern_number) {
                    if (i - pattern_lenght[num] + 1 - start_pat_pos[num] >= 0) {
                        if(i + question_mark_end < static_cast<int>(text.size())){
                            positions[i - pattern_lenght[num] + 1 - start_pat_pos[num]]++;
                        }
                    }
                }
            }
            temp = GetCompressedLink(temp_shr);
            temp_shr = temp.lock();
        }
    }
    // Count pattern with ? positions in text.
    for (int i = 0; i < static_cast<int>(positions.size()); i++) {
        if (positions[i] == start_pat_pos.size()) {
            result.push_back(i);
        }
    }
    return result;
}

// Parse pattern with ? to pattern without ? (little patterns)
std::pair<vector<int>, int> Parser(const string& pattern, vector<string>& little_patterns) {
    int question_mark_end = 0;
    vector<int> start_pat_pos;
    string little_pattern;
    for (int i = 0; i < pattern.size(); i++) {
        auto symbol = pattern[i];
        if (symbol != DIVIDER) {
            little_pattern.push_back(symbol);
            question_mark_end = 0;
        } else {
            question_mark_end += 1;
            if (!little_pattern.empty()) {
                little_patterns.push_back(little_pattern);
                start_pat_pos.push_back(i - static_cast<int>(little_pattern.size()));
                little_pattern = "";
            }
        }
    }
    if (!little_pattern.empty()) {
        little_patterns.push_back(little_pattern);
        start_pat_pos.push_back(static_cast<int>(pattern.size()) - static_cast<int>(little_pattern.size()));
        question_mark_end = 0;
    }
    return make_pair(start_pat_pos, question_mark_end);
}

int main() {
    std::ifstream fin("/home/sonya/CLionProjects/Axo-Karasik/input.txt");
    string pattern;
    string text;
    fin >> pattern;
    fin >> text;
    vector<string> little_patterns;

    std::pair<vector<int>, int> pars = Parser(pattern, little_patterns);
    vector<int> start_pat_pos = pars.first;
    int question_mark_end = pars.second;

    CBorTree borTree(little_patterns, std::move(text), question_mark_end);
    vector<int> result = borTree.FindPattern(start_pat_pos);

    for(auto res: result){
        std::cout << res << " ";
    }
    return 0;
}