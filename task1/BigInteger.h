#include <string>
#include <iostream>
#include <vector>

using std::cout;
using std::cin;
using std::vector;
using std::string;
using std::swap;

#define MAX_DIGIT 100000000
#define BASE 10 // work only with 10 base

class BigInteger {
public:
    BigInteger();
    ~BigInteger() {};
    BigInteger( BigInteger&& value ) noexcept;
    BigInteger( const BigInteger& value ) noexcept;

    BigInteger( const string& value );
    BigInteger( int value );

    // Assignment operators
    BigInteger& operator=( const BigInteger& value ) noexcept;
    BigInteger& operator=( BigInteger&& value ) noexcept;

    string toString() const;

    // Logical operators
    bool operator==( const BigInteger& right ) const;
    bool operator!=( const BigInteger& right ) const;
    bool operator<( const BigInteger& right ) const;
    bool operator<=( const BigInteger& right ) const;
    bool operator>( const BigInteger& right ) const;
    bool operator>=( const BigInteger& right ) const;

    //  Unary operator
    BigInteger operator-() const;

    //  Pre/post increment/decrement
    BigInteger& operator++();
    BigInteger& operator--();

    BigInteger operator++( int );
    BigInteger operator--( int );

    //  Binary operators
    friend BigInteger operator+( const BigInteger& left, const BigInteger& right );
    friend BigInteger operator-( const BigInteger& left, const BigInteger& right );
    friend BigInteger operator*( const BigInteger& left, const BigInteger& right );
    friend BigInteger operator/( const BigInteger& left, const BigInteger& right );
    friend BigInteger operator%( const BigInteger& left, const BigInteger& right );

    //  Compound assignment operators
    BigInteger& operator+=( const BigInteger& other );
    BigInteger& operator-=( const BigInteger& other );
    BigInteger& operator*=( const BigInteger& other );
    BigInteger& operator/=( const BigInteger& other );
    BigInteger& operator%=( const BigInteger& other );

    //  type conversion
    explicit operator bool() const;

    // input/output
    friend std::ostream& operator<<( std::ostream &stream, const BigInteger& value );
    friend std::istream& operator>>( std::istream &stream, BigInteger& value );

private:
    bool negative;
    vector<int> digits; // 8 digits to a vector element MAX_DIGIT when all nulls

    string reverseStr(const string value) const;
    string fromIntToStr(const int value, bool add_leading_zeros) const;
    const BigInteger absValue() const;
    void addNumber(const BigInteger& value);
    void unsignedSubtract(const BigInteger& other);
    void multiplyByInt(const int value);
};

string BigInteger::reverseStr(const string value) const{
    string temp_value = value;
    string result = "";
    int size = static_cast<int>(temp_value.size() - 1);
    for(int i = size; i >= 0; i--){
        result += temp_value[i];
    }
    return result;
}

string BigInteger::fromIntToStr(const int value, bool add_leading_zeros) const{
    string result = "";
    int temp_value = value;
    int i = 0;
    while(temp_value && i < 8){
        i++;
        int toAdd = temp_value % BASE;
        temp_value /= BASE;
        result += static_cast<char>(toAdd + '0');
    }
    if(add_leading_zeros){
        for(; i < 8; i++){
            result += '0';
        }
    }
    result = reverseStr(result);
    return result;
}

const BigInteger BigInteger::absValue() const {
    BigInteger absValue;
    absValue.negative = false;
    absValue.digits = digits;
    return absValue;
}

void BigInteger::addNumber(const BigInteger& value){
    int num_size = static_cast<int>(digits.size());
    int value_size = static_cast<int>(value.digits.size());
    int carry = 0;
    for(int i = 0; i < std::max(num_size, value_size) || carry; i++){
        int left, right;
        if(i < num_size){
            left = digits[i];
        }else{
            left = 0;
        }
        if(i < value_size){
            right = value.digits[i];
        }else{
            right = 0;
        }
        int new_left = 0;
        for(int j = 1; j < MAX_DIGIT; j*=BASE){
            int toAdd = carry;
            toAdd += left%BASE + right%BASE;
            left /= BASE;
            right /= BASE;
            new_left += j*(toAdd % BASE);
            carry = toAdd / BASE;
        }
        if(new_left == 0) {
            new_left = MAX_DIGIT;
        }
        if (i < num_size) {
            digits[i] = new_left;
        } else {
            digits.push_back(new_left);
        }
    }
}

// left > right : left = left - right
void BigInteger::unsignedSubtract(const BigInteger& other) {
    int num_size = static_cast<int>(digits.size());
    int right_size = static_cast<int>(other.digits.size());
    int carry = 0;
    for (int i = 0; i < std::max(num_size, right_size) || carry; ++i) {
        int left, right;
        if(i < num_size){
            left = digits[i];
        }else{
            left = 0;
        }
        if(i < right_size){
            right = other.digits[i];
        }else{
            right = 0;
        }
        int new_left = 0;
        for(int j = 1; j < MAX_DIGIT; j*=BASE){
            int toAdd = -carry;
            toAdd += left%BASE - right%BASE;
            left /= BASE;
            right /= BASE;
            if(toAdd < 0){
                toAdd += BASE;
                carry = 1;
            }else{
                carry = 0;
            }
            new_left += j*(toAdd % BASE);
        }
        if(!(num_size == 0 && i < num_size)){
            if (i < num_size) {
                digits[i] = new_left;
            } else {
                digits.push_back(new_left);
            }
        }
    }

    //remove leading zeros
    while (!digits.empty() && !digits.back()) {
        digits.pop_back();
    }
    if (digits.empty()) {
        negative = false;
    }
}

void BigInteger::multiplyByInt(const int value) {
    int temp_value = value;
    if (!temp_value || !*this) {
        digits.clear();
        negative = false;
        return;
    }
    if (temp_value < 0) {
        temp_value *= -1;
        negative = !negative;
    }
    int n = static_cast<int>(digits.size());
    int carry = 0;
    int new_left = 0;
    for (int i = 0; i < n || carry; ++i) {
        int left = 0;
        if(i < n){
            left = digits[i];
        }
        new_left = 0;
        for(int j = 1; j < MAX_DIGIT; j*=BASE) {
            int toAdd = carry;
            toAdd += left % BASE * temp_value;
            new_left += (toAdd % BASE) * j;
            carry = toAdd / BASE;
            left /= BASE;
        }
        if(new_left == 0) {
            new_left = MAX_DIGIT;
        }
        if (i < n) {
            digits[i] = new_left;
        } else {
            digits.push_back(new_left);
        }
    }
}

//---------------------------------------------------------------------------------------

// Constructors

BigInteger::BigInteger() {
    negative = false;
}

BigInteger::BigInteger( int value) {
    int temp_value = value;
    negative = (temp_value < 0);
    if(negative){
        temp_value *= -1;
    }
    while(temp_value){
        int toAdd = 0;
        for(int i = 1; i <= MAX_DIGIT; i *= BASE){
            toAdd += i*(temp_value % BASE);
            temp_value /= BASE;
            if(!temp_value){
                digits.push_back(toAdd);
                return;
            }
        }
        digits.push_back(toAdd);
    }
}

BigInteger::BigInteger( BigInteger&& value ) noexcept{
    swap(value.negative, negative);
    swap(value.digits, digits);
}

BigInteger::BigInteger( const BigInteger& value ) noexcept{
    negative = value.negative;
    digits = value.digits;
}

BigInteger::BigInteger(const string& value) {
    negative = false;
    if (value != "0") {
        int digit = 1;
        int toAdd = 0;
        int n = static_cast<int>(value.size());
        for (int i = n - 1; i >= 0; --i) {
            if(value[i] == '-'){
                if(i != 0){
                    cout << "Error: incorrect number string";
                    exit(1);
                }
                negative = true;
                break;
            }else{
                if(!isdigit(value[i])){
                    cout << "Error: incorrect number string.";
                    exit(1);
                }
            }
            if(digit >= MAX_DIGIT){
                digit = 1;
                if(toAdd == 0){
                    toAdd = MAX_DIGIT;
                }
                digits.push_back(toAdd);
                toAdd = 0;
            }
            toAdd += (value[i] - '0') * digit;
            digit *= BASE;
        }
        if(toAdd){
            digits.push_back(toAdd);
        }
    }
}

//---------------------------------------------------------------------------------------

// Assignment operators

BigInteger& BigInteger::operator=(const BigInteger& value) noexcept {
    if (this != &value) {
        negative = value.negative;
        digits = value.digits;
    }
    return *this;
}

BigInteger& BigInteger::operator=(BigInteger&& value) noexcept {
    std::swap(value.negative, negative);
    std::swap(value.digits, digits);
    return *this;
}

//---------------------------------------------------------------------------------------

string BigInteger::toString() const {
    if (digits.empty()) {
        return "0";
    }
    string s = negative ? "-" : "";
    int n = static_cast<int>(digits.size());
    bool add_leading_zeros = false;
    for (int i = n - 1; i >= 0; --i) {
        int temp_element = digits[i];
        string temp_element_str = fromIntToStr(temp_element, add_leading_zeros);
        s += temp_element_str;
        add_leading_zeros = true;
    }
    return s;
}

//---------------------------------------------------------------------------------------

// Logical operators

bool BigInteger::operator ==(const BigInteger& right) const {
    return negative == right.negative && digits == right.digits;
}

bool BigInteger::operator !=(const BigInteger& right) const {
    return !(*this == right);
}

bool BigInteger::operator <(const BigInteger& right) const {
    if (negative != right.negative) {
        return negative;
    }
    if (digits.size() != right.digits.size()) {
        return (digits.size() < right.digits.size()) ^ negative;
    }
    int n = static_cast<int>(digits.size());
    for (int i = n - 1; i >= 0; --i) {
        if (digits[i] != right.digits[i]) {
            return (digits[i] < right.digits[i]) ^ negative;
        }
    }
    return false;
}

bool BigInteger::operator <=(const BigInteger& right) const {
    return !(*this > right);
}

bool BigInteger::operator >(const BigInteger& right) const {
    return right < *this;
}

bool BigInteger::operator >=(const BigInteger& right) const {
    return !(*this < right);
}

//---------------------------------------------------------------------------------------

//  Unary operator

BigInteger BigInteger::operator-() const{
    BigInteger neg(*this);
    if (neg) {
        neg.negative = !neg.negative;
    }
    return neg;
}

//---------------------------------------------------------------------------------------

//  Pre/post increment/decrement

BigInteger& BigInteger::operator++(){
    return *this += BigInteger(1);
}
BigInteger& BigInteger::operator--(){
    return *this -= BigInteger(1);
}

BigInteger BigInteger::operator++( int ){
    BigInteger old(*this);
    ++(*this);
    return old;
}

BigInteger BigInteger::operator--( int ){
    BigInteger old(*this);
    --(*this);
    return old;
}

//---------------------------------------------------------------------------------------

//  Binary operators

BigInteger operator+( const BigInteger& left, const BigInteger& right ){
    return BigInteger(left) += right;
}

BigInteger operator-( const BigInteger& left, const BigInteger& right ){
    return BigInteger(left) -= right;
}

BigInteger operator*( const BigInteger& left, const BigInteger& right ){
    return BigInteger(left) *= right;
}

BigInteger operator/(const BigInteger& left, const BigInteger& right) {
    return BigInteger(left) /= right;
}
BigInteger operator%(const BigInteger& left, const BigInteger& right) {
    return BigInteger(left) %= right;
}

//---------------------------------------------------------------------------------------

//  Compound assignment operators

BigInteger& BigInteger::operator+=( const BigInteger& other ){
    if (!*this || !other) {
        if (!*this) {
            *this = other;
        }
        return *this;
    }
    if (negative == other.negative) {
        addNumber(other);
        return *this;
    }
    if (absValue() == other.absValue()) {
        digits.clear();
        negative = false;
        return *this;
    } else if (negative) {
        if (absValue() < other.absValue()) {
            BigInteger rightTemp(other);
            std::swap(digits, rightTemp.digits);
            unsignedSubtract(rightTemp);
            negative = false;
        } else {
            unsignedSubtract(other);
            negative = true;
        }
    } else {
        if (absValue() < other.absValue()) {
            BigInteger rightTemp(other);
            std::swap(digits, rightTemp.digits);
            unsignedSubtract(rightTemp);
            negative = true;
        } else {
            unsignedSubtract(other);
            negative = false;
        }
    }
    return *this;
}

BigInteger& BigInteger::operator-=(const BigInteger& other) {
    return *this += -other;
}

BigInteger& BigInteger::operator*=(const BigInteger& other) {
    if (!*this || !other) {
        digits.clear();
        negative = false;
        return *this;
    }
    int other_size = static_cast<int>(other.digits.size());
    int this_size = static_cast<int>(digits.size());

    BigInteger toAdd(this_size < other_size ? other : *this), prod;
    toAdd.negative = false;
    bool prodSign = (negative != other.negative);
    std::vector<int> left = digits;
    digits.clear();
    negative = false;
    for (int i = 0; i < std::min(this_size, other_size); i++) {
        int num1 = left[i];
        int num2 = other.digits[i];
        for(int j = 1; j < MAX_DIGIT; j*=BASE){
            BigInteger addend(toAdd);
            addend.multiplyByInt(this_size < other_size ? num1%BASE : num2%BASE);
            num1 /= BASE;
            num2 /= BASE;
            *this += addend;
            toAdd.multiplyByInt(BASE);
        }
    }
    negative = *this ? prodSign : false;
    return *this;
}

BigInteger& BigInteger::operator/=(const BigInteger& other) {
    if (!*this) {
        return *this;
    }
    if(!other){
        cout << "Error: division by zero";
        exit(1);
    }
    BigInteger z, absRhs = other.absValue();
    bool div_sign = (negative != other.negative);
    int n = static_cast<int>(digits.size());
    std::vector<int> lhs = digits;
    digits.clear();
    negative = false;
    bool divides = false;
    bool add_leading_nulls = false;
    for (int i = 0; i < n; i++) {
        int temp = lhs[n - i - 1];
        string temp_str = fromIntToStr(temp, add_leading_nulls);
        add_leading_nulls = true;
        for(int l = 0; l < static_cast<int>(temp_str.size()); l++){
            z.multiplyByInt(BASE);
            z += BigInteger(temp_str[l] - '0');
            if (divides || z >= absRhs) {
                divides = true;
                multiplyByInt(BASE);
                while (z >= absRhs) {
                    z -= absRhs;
                    ++(*this);
                }
            }
        }
    }
    negative = *this ? div_sign : false;
    return *this;
}

BigInteger& BigInteger::operator%=(const BigInteger& other) {
    if (!*this) {
        return *this;
    }
    if(!other){
        cout << "Error: division by zero";
        exit(1);
    }
    BigInteger quotient(*this);
    quotient /= other;
    BigInteger multipleToModulus(other);
    multipleToModulus *= quotient;
    return *this -= multipleToModulus;
}

//---------------------------------------------------------------------------------------

// type conversion

BigInteger::operator bool() const {
    return !digits.empty();
}

//---------------------------------------------------------------------------------------

// input/output

std::istream& operator>>(std::istream &stream, BigInteger& value) {
    std::string s;
    stream >> s;
    value = BigInteger(s);
    return stream;
}

std::ostream& operator<<(std::ostream &stream, const BigInteger& value) {
    stream << value.toString();
    return stream;
}