#include <algorithm>
#include <iostream>
#include <queue>

using std::cin;
using std::cout;
using std::vector;
using std::queue;

struct Point {
    explicit Point(double new_x, double new_y, double new_z, int position);
    explicit Point(double new_x = 0.0, double new_y = 0.0, double new_z = 0.0);
    friend const Point operator-(const Point& a1, const Point& a2);
    friend double scalar(const Point& a1, const Point& a2);
    friend double cos(const Point& a, const Point& b);
    friend Point vectorProduct(const Point& a1, const Point& a2);

    double length() const;

    double x;
    double y;
    double z;
    // position in the array
    int position;
};

Point::Point(double new_x, double new_y, double new_z, int new_position)
        :x(new_x)
        ,y(new_y)
        ,z(new_z)
        ,position(new_position)
{}

Point::Point(double new_x, double new_y, double new_z)
        :x(new_x)
        ,y(new_y)
        ,z(new_z)
        ,position(-1)
{}

const Point operator-(const Point& a1, const Point& a2) {
    return Point(a1.x - a2.x, a1.y - a2.y, a1.z - a2.z);
}

double scalar(const Point& a1, const Point& a2) {
    return a1.x * a2.x + a1.y * a2.y + a1.z * a2.z;
}

double cos(const Point& a, const Point& b) {
    double cos;
    cos = scalar(a, b)/(a.length()*b.length());
    return cos;
}

Point vectorProduct(const Point& a1, const Point& a2) {
    return Point(a1.y*a2.z - a1.z*a2.y,
            a1.z*a2.x - a1.x*a2.z ,a1.x * a2.y - a2.x * a1.y);
}

double Point::length() const {
    return sqrt(x*x + y*y + z*z);
}

struct CFace{
    CFace(Point& new_point1, Point& new_point2, Point& new_point3);
    Point a;
    Point b;
    Point c;
    Point normal;
    double checkOfHalfSpace(Point& p);
    // face equation Ax + By + Cz + D = 0
    double A, B, C, D;
};

CFace::CFace(Point& new_point1, Point& new_point2, Point& new_point3)
        :a(new_point1)
        ,b(new_point2)
        ,c(new_point3)
{
    Point p1 = b - a;
    Point p2 = c - a;
    normal = vectorProduct(p1, p2);
    A = normal.x;
    B = normal.y;
    C = normal.z;
    D = -a.x * A - a.y * B - a.z * C;
}

double CFace::checkOfHalfSpace(Point& p) {
    return A*p.x + B*p.y + C*p.z + D;
}

struct CSegment{
    CSegment(Point& new_a, Point& new_b);
    Point a;
    Point b;
};

CSegment::CSegment(Point& new_a, Point& new_b)
        :a(new_a)
        ,b(new_b)
{}

class ConvexHull{
public:
    explicit ConvexHull(vector<Point>& new_set_of_points);
    void findConvexHull();
    void printAnswer();
private:
    int number_of_points;
    vector<Point> set_of_points;
    vector<vector<bool>> contour;
    queue<CFace> queueOfFaces;
    vector<vector<int>> answer;

    void updateEdge(CSegment edge);
    // update contour of convex hull
    void updateContour(CFace& face_to_add);
    void findNextFace(CFace& current_face);
    void considerASegment(CSegment edge, CFace& current_face);
    void addFirstFace();
};

ConvexHull::ConvexHull(vector<Point>& new_set_of_points) :
        set_of_points(std::move(new_set_of_points))
{
    number_of_points = static_cast<int>(set_of_points.size());
    contour.resize(set_of_points.size());
    for(int i = 0; i < number_of_points; i++){
        contour[i].resize(set_of_points.size());
    }
}

void ConvexHull::findConvexHull() {
    addFirstFace();
    while(!queueOfFaces.empty()){
        CFace current_face = queueOfFaces.front();
        queueOfFaces.pop();
        findNextFace(current_face);
    }
}

void ConvexHull::updateEdge(CSegment edge) {
    if(contour[edge.b.position][edge.a.position]){
        contour[edge.b.position][edge.a.position] = false;
    }else{
        contour[edge.a.position][edge.b.position] = true;
    }
}

void ConvexHull::updateContour(CFace& face_to_add) {
    updateEdge(CSegment(face_to_add.a, face_to_add.b));
    updateEdge(CSegment(face_to_add.b, face_to_add.c));
    updateEdge(CSegment(face_to_add.c, face_to_add.a));

}

void ConvexHull::findNextFace(CFace& current_face){
    considerASegment(CSegment(current_face.a, current_face.b), current_face);
    considerASegment(CSegment(current_face.b, current_face.c), current_face);
    considerASegment(CSegment(current_face.c, current_face.a), current_face);
}

void ConvexHull::considerASegment(CSegment edge, CFace& current_face) {
    if(contour[edge.a.position][edge.b.position]){
        Point c;
        double max_cos = -1;
        for(int i = 0; i < number_of_points; i++){
            if(i != current_face.a.position && i != current_face.b.position && i != current_face.c.position){
                CFace new_face = CFace(edge.b, edge.a, set_of_points[i]);
                double new_cos = cos(new_face.normal, current_face.normal);
                if(new_cos > max_cos){
                    max_cos = new_cos;
                    c = set_of_points[i];
                }
            }
        }
        CFace face_to_add = CFace(edge.b, edge.a, c);
        updateContour(face_to_add);
        queueOfFaces.push(face_to_add);
        vector<int> face_to_add_v;
        face_to_add_v.push_back(face_to_add.a.position);
        face_to_add_v.push_back(face_to_add.b.position);
        face_to_add_v.push_back(face_to_add.c.position);
        answer.push_back(face_to_add_v);
    }
}

void ConvexHull::addFirstFace() {
    Point first_point = set_of_points[0];
    for(int i = 0; i < number_of_points; i++){
        Point current = set_of_points[i];
        if(current.z < first_point.z){
            first_point = current;
        }
    }
    double min_cos = 1;
    Point second_point;
    // choose a second_point != first_point
    for(int i = 0; i < number_of_points; i++){
        if(i != first_point.position){
            second_point = set_of_points[i];
        }
    }
    for(int i = 0; i < number_of_points; i++){
        Point current = set_of_points[i];
        double cos1 = cos(Point(0,0,1), current - first_point);
        if(cos1 < min_cos ){
            min_cos = cos1;
            second_point = current;
        }
    }
    Point third_point;
    for(int i = 0; i < number_of_points; i++){
        Point current;

        if(!(i == second_point.position || i == first_point.position)) {
            current = set_of_points[i];
            CFace current_face = CFace(first_point, second_point, current);
            bool positively = false, negatively = false;
            for (int i = 0; i < number_of_points; i++) {
                // check the face
                double side = current_face.checkOfHalfSpace(set_of_points[i]);
                if (side > 0) {
                    positively = true;
                }
                if (side < 0) {
                    negatively = true;
                }
            }
            if (!(positively && negatively)) {
                third_point = current;
                if (positively) {
                    std::swap(third_point, second_point);
                    current_face = CFace(first_point, second_point, third_point);
                }
                contour[first_point.position][second_point.position] = true;
                contour[second_point.position][third_point.position] = true;
                contour[third_point.position][first_point.position] = true;
                queueOfFaces.push(current_face);
                vector<int> face_to_add;
                face_to_add.push_back(current_face.a.position);
                face_to_add.push_back(current_face.b.position);
                face_to_add.push_back(current_face.c.position);
                answer.push_back(face_to_add);
                break;
            }
        }
    }
}

void cyclic_shift(vector<int>& cycle){
    vector<int> new_cycle;
    int min = 0;
    for(int i = 0; i < 3; i++){
        if(cycle[i] < cycle[min]){
            min = i;
        }
    }
    for(int i = min; i < 3; i++){
        new_cycle.push_back(cycle[i]);
    }
    for(int i = 0; i < min; i++){
        new_cycle.push_back(cycle[i]);
    }
    cycle = new_cycle;
}

bool comparator(const vector<int>& a, const vector<int>& b){
    return a[0] < b[0] || a[0] == b[0] && a[1] < b[1] || a[0] == b[0] && a[1] == b[1] && a[2] < b[2];
}

void ConvexHull::printAnswer() {
    cout << answer.size() << '\n';
    for(int i = 0; i < static_cast<int>(answer.size()); i++){
        cyclic_shift(answer[i]);
    }
    std::sort(answer.begin(), answer.end(), comparator);
    for(int i = 0; i < static_cast<int>(answer.size()); i++){
        cout << "3" << " " << answer[i][0] << " " << answer[i][1] << " " << answer[i][2] << '\n';
    }
}


int main() {
    unsigned int number_of_tests;
    cin >> number_of_tests;
    vector<vector<Point>> tests;
    tests.resize(number_of_tests);
    for (int i = 0; i < number_of_tests; i++) {
        unsigned int number_of_dots;
        cin >> number_of_dots;
        vector<Point> test;
        test.resize(number_of_dots);

        for (int j = 0; j < number_of_dots; j++) {
            int x, y, z;
            cin >> x >> y >> z;
            auto point = Point(x, y, z, j);
            test[j] = point;
        }
       	
        tests[i] = test;
        ConvexHull convex_hull = ConvexHull(test);
        convex_hull.findConvexHull();
        convex_hull.printAnswer();
    }
}
