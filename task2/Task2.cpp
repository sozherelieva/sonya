#include <algorithm>
#include <fstream>
#include <iostream>


class SubstringSearch {
public:
    explicit SubstringSearch(const std::string& new_text);
    SubstringSearch(std::vector<int>& some_function, bool prefix);

    void PrefixFunction();
    void FromPrefixToZ();
    void ZFunction();
    void FromZToPrefix();
    void FromZToText();
    void FromPrefixToText();

    void PrintZ();
    void PrintPrefix();
    void PrintText();

private:
    std::string text;

    std::vector<int> prefix_function;
    int text_lenght;
    int previous_prefix;
    int Prefix(char character);

    std::vector<int> z_function;
    int left;
    int right;
};

void SubstringSearch::FromZToText() {
    text = "";
    size_t alphabet_size = 26;

    std::vector<std::vector<bool>> blocked_characters;
    blocked_characters.resize(z_function.size());
    for (int i = 0; i < static_cast<int>(z_function.size()); i++) {
        blocked_characters[i].resize(alphabet_size);
        for (int j = 0; j < alphabet_size; j++) {
            blocked_characters[i][j] = false;
        }
    }

    z_function[0] = 0;
    if (z_function.size() > 0) {
        text += "a";
    }

    int i = 1;
    while (i < static_cast<int>(z_function.size())) {
        if (z_function[i] == 0) {
            blocked_characters[i][0] = true;
            for (int j = 0; j < alphabet_size; j++) {
                if (!blocked_characters[i][j]) {
                    text += static_cast<char>(static_cast<int>('a') + j);
                    break;
                }
            }
            i++;
        } else {
            for (int j = 0; j < z_function[i]; j++) {
                text += text[j];
            }
            // Block characters according to z_func.
            for (int j = 0; j < z_function[i]; j++) {
                if (i + j + z_function[i + j] < static_cast<int>(z_function.size())) {
                    if (z_function[i + j] > 0) {
                        blocked_characters[i + j + z_function[i + j]][text[z_function[i + j]] - 'a'] = true;
                    }
                }
            }
            i += z_function[i];
            if (i >= static_cast<int>(z_function.size())) {
                break;
            }
            blocked_characters[i][text[z_function[i]] - 'a'] = true;
        }
    }
    z_function[0] = static_cast<int>(prefix_function.size());
}

void SubstringSearch::FromPrefixToText() {
    text = "";
    size_t alphabet_size = 26;

    std::vector<std::vector<bool>> blocked_characters;
    blocked_characters.resize(prefix_function.size());
    for (int i = 0; i < static_cast<int>(prefix_function.size()); i++) {
        blocked_characters[i].resize(alphabet_size);
        for (int j = 0; j < alphabet_size; j++) {
            blocked_characters[i][j] = false;
        }
    }

    if (prefix_function.size() > 0) {
        text += "a";
    }

    int i = 1;
    while (i < static_cast<int>(prefix_function.size())) {
        if (prefix_function[i] == 0) {
            // Block characters according to a prefix_function properties.
            blocked_characters[i][0] = true;
            int prefix = prefix_function[i - 1];
            while (prefix > 0) {
                blocked_characters[i][text[prefix] - 'a'] = true;
                prefix = prefix_function[prefix - 1];
            }
            // Find the minimum among unlocked characters.
            for (int j = 0; j < alphabet_size; j++) {
                if (!blocked_characters[i][j]) {
                    text += static_cast<char>(static_cast<int>('a') + j);
                    i++;
                    break;
                }
            }
        } else {
            if (prefix_function[i] - 1 < text.size()) {
                text += text[prefix_function[i] - 1];
                i++;
            }
        }
    }
}

void SubstringSearch::FromZToPrefix() {
    prefix_function.resize(z_function.size());
    for (int i = 0; i < static_cast<int>(z_function.size()); i++) {
        prefix_function[i] = 0;
    }
    for (int i = 1; i < static_cast<int>(z_function.size()); i++) {
        if (z_function[i]) {
            if (prefix_function[i + z_function[i] - 1] < z_function[i]) {
                prefix_function[i + z_function[i] - 1] = z_function[i];
            }
        }
        for (int j = i + z_function[i] - 2; j >= i; j--) {
            if (prefix_function[j]) {
                break;
            }
            prefix_function[j] = j - i + 1;
        }
    }
}

void SubstringSearch::FromPrefixToZ() {
    z_function.resize(prefix_function.size());
    for (int i = 0; i < static_cast<int>(prefix_function.size()); i++) {
        z_function[i] = 0;
    }
    for (int i = 1; i < prefix_function.size(); i++) {
        if (prefix_function[i]) {
            z_function[i - prefix_function[i] + 1] = prefix_function[i];
        }
    }
    z_function[0] = static_cast<int>(prefix_function.size());

    int i = 1;
    while (i < prefix_function.size()) {
        int j;
        for ( j = 1; j < z_function[i]; j++) {
            int temp = std::min(z_function[j], z_function[i] - j);
            if (temp >= z_function[i + j]) {
                z_function[i + j] = temp;
            } else {
                break;
            }
        }
        i += j;
    }
}

void SubstringSearch::PrintZ() {
    for (auto result : z_function) {
        std::cout << result << " ";
    }
    std::cout << '\n';
}

void SubstringSearch::PrintPrefix() {
    for (auto result : prefix_function) {
        std::cout << result << " ";
    }
    std::cout << '\n';
}

void SubstringSearch::PrintText() {
    std::cout << text;
    std::cout << '\n';
}

void SubstringSearch::ZFunction() {
    for (int i = 1; i < text.size(); i++) {
        if (i > right) {
            int j = 0;
            while (text[i + j] == text[j] && i + j < text.size()) {
                j++;
            }
            z_function[i] = j;
            if (right < i + j) {
                right = i + j;
                left = i;
            }
        } else {
            if ((z_function[i - left] + i) >= right) {
                int j = 0;
                while (text[right + j] == text[right - i + j] && (right + j) < text.size()) {
                    j++;
                }
                z_function[i] = j + right - i;
                if (right < j + right) {
                    right = j + right;
                    left = i;
                }
            } else {
                z_function[i] = z_function[i - left];
                if (right < i + z_function[i - left]) {
                    right = i + z_function[i - left];
                    left = i;
                }
            }
        }
    }
}

void SubstringSearch::PrefixFunction() {
    int i = 0;
    for (auto character : text) {
        prefix_function[i] = Prefix(character);
        i++;
    }
}

int SubstringSearch::Prefix(const char character) {
    // Try to find smaller prefixes of the string.
    while (character != text[previous_prefix] && previous_prefix != 0) {
        previous_prefix = prefix_function[previous_prefix - 1];
    }

    if (character == text[previous_prefix]) {
        if (text_lenght == 0) {
            previous_prefix = 0;
        } else {
            previous_prefix += 1;
        }
    }
    text_lenght += 1;
    return previous_prefix;
}

SubstringSearch::SubstringSearch(const std::string& new_text) {
    text_lenght = 0;
    previous_prefix = 0;
    const_cast<std::string&>(text) = new_text;

    // Counting text prefix function.
    prefix_function.resize(text.size());

    right = 0;
    left = 0;
    z_function.resize(text.size());
    for (int i = 0; i < text.size(); i++) {
        z_function.push_back(0);
        prefix_function.push_back(0);
    }
    z_function[0] = static_cast<int>(text.size());
}

SubstringSearch::SubstringSearch(std::vector<int>& some_function, bool prefix) {
    text_lenght = 0;
    previous_prefix = 0;
    left = 0;
    right = 0;
    if (prefix) {
        text.clear();
        prefix_function = std::move(some_function);
        FromPrefixToText();
    } else {
        text.clear();
        z_function = std::move(some_function);
        FromZToText();
    }
}

int main() {
    std::ifstream fin("/home/sonya/CLionProjects/PrefixSuffixAllTheStuff/input.txt");
    std::vector<int> some_function;
    int number;
    while (fin >> number) {
        some_function.push_back(number);
    }


    // 1 - prefix_function; 0 - z_function
    SubstringSearch SubstringSearch(some_function, false);
    SubstringSearch.PrintText();

    //std::string text; int number;
    //std::cin >> text;

    //SubstringSearch SubstringSearch(text);
    //SubstringSearch.ZFunction();
    //SubstringSearch.PrintZ();

    //SubstringSearch.PrefixFunction();
    //SubstringSearch.PrintPrefix();

    //SubstringSearch.FromPrefixToZ();
    //SubstringSearch.PrintZ();

    //SubstringSearch.FromZToPrefix();
    //SubstringSearch.PrintPrefix();
    //fin.close();
    return 0;
}
