#include <iostream>
#include <cmath>
#include <vector>


using std::cin;
using std::vector;
using std::min;

#define SMALL_NUM 0.0000009
#define INFINITY 1000000

struct Point {
    explicit Point(double new_x = 0.0, double new_y = 0.0, double new_z = 0.0);

    friend const Point operator+(const Point& a1, const Point& a2);

    friend const Point operator-(const Point& a1, const Point& a2);

    const Point operator-(const Point& a1);

    friend const Point operator*(double num, const Point& a1);

    friend double scalar(const Point& a1, const Point& a2);

    double x;
    double y;
    double z;
};

Point::Point(double new_x, double new_y, double new_z)
    :x(new_x)
    ,y(new_y)
    ,z(new_z) //prefer initialisation lists (Meyers Effective C++)
{}

const Point operator+(const Point& a1, const Point& a2) {
    return Point(a1.x + a2.x, a1.y + a2.y, a1.z + a2.z);
}

const Point operator-(const Point& a1, const Point& a2) {
    return Point(a1.x - a2.x, a1.y - a2.y, a1.z - a2.z);
}

const Point Point::operator-(const Point& a1) {
    return Point(-a1.x, -a1.y, -a1.z);
}

double scalar(const Point& a1, const Point& a2) {
    return a1.x * a2.x + a1.y * a2.y + a1.z * a2.z;
}

const Point operator*(double num, const Point& a1) {
    return Point(a1.x * num, a1.y * num, a1.z * num);
}

struct Segment {
    Segment(const Point& new_a, const Point& new_b);

    Point a;
    Point b;
};

Segment::Segment(const Point& new_a, const Point& new_b)
    :a(new_a)
    ,b(new_b)
{}

class DistanceBetweenSegments{
public:
    DistanceBetweenSegments(const Segment& segment1, const Segment& segment2);
    double calculateDistance();
private:
    double distance(double t, double s);

    double a;
    double b;
    double c;
    double d;
    double e;
    double f;
};

DistanceBetweenSegments::DistanceBetweenSegments(const Segment& segment1, const Segment& segment2) {
    Point v1 = segment1.b - segment1.a;
    Point v2 = segment2.b - segment2.a;
    Point v3 = segment1.a - segment2.a;
    // distance between two points on the segment - r
    // r = as^2 - 2bst + ct^2 + 2ds - 2et + f
    a = scalar(v1, v1);
    b = scalar(v1, v2);
    c = scalar(v2, v2);
    d = scalar(v1, v3);
    e = scalar(v2, v3);
    f = scalar(v3, v3);
}

double DistanceBetweenSegments::distance (double t, double s) {
    return a * s * s - 2 * b * s * t + c * t * t + 2 * d * s - 2 * e * t + f;
}


double DistanceBetweenSegments::calculateDistance() {
    double denominator = a * c - b * b; // always >= 0
    double s, t, s1, s2, t1, t2;
    double result = INFINITY;
    if (denominator < SMALL_NUM) { // segments are parallel
        s = 0.0;
        t = e / c;
    } else {
        s = (b * e - c * d) / denominator;
        t = (a * e - b * d) / denominator;
    }
    s1 = -d / a;
    s2 = (b - d) / a;
    t1 = e / c;
    t2 = (b + e) / c;

    if (t >= 0 && t <= 1 && s >= 0 && s <= 1) {
        result = min(result, distance(t, s));
    }

    if (t1 >= 0 && t1 <= 1) {
        result = min(result, distance(t1, 0));
    }

    if (t2 >= 0 && t2 <= 1) {
        result = min(result, distance(t2, 1));
    }

    if (s1 >= 0 && s1 <= 1) {
        result = min(result, distance(0, s1));
    }

    if (s2 >= 0 && s2 <= 1) {
        result = min(result, distance(1, s2));
    }

    result = min(result, distance(0, 0));
    result = min(result, distance(1, 0));
    result = min(result, distance(0, 1));
    result = min(result, distance(1, 1));

    return sqrt(result);
}

int main() {
    vector<Point> points;
    for (int i = 0; i < 4; i++) {
        int x, y, z;
        cin >> x >> y >> z;
        points.emplace_back(Point(x, y, z));
    }

    vector<Segment> segments;
    for (int i = 0; i < 2; i++) {
        segments.emplace_back(Segment(points[2 * i], points[2 * i + 1]));
    }

    Segment segment1 = segments[0];
    Segment segment2 = segments[1];

    DistanceBetweenSegments distance = DistanceBetweenSegments(segment1, segment2);
    std::cout.precision(20);
    std::cout << distance.calculateDistance();

    return 0;
}